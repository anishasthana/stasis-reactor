// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="what-is-stasis-reactor"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= What is Stasis Reactor?
:stasis-weight: 10
//In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly.
//Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.

Stasis Reactor is a project for presenting a series of https://asciidoc.org[AsciiDoc]
files in a statically generated website with a https://reactjs.org[React]-based
user interface.

The reasoning for wrapping a static site with React is to encourage deeper
information interactions with the documents without the need for a server-based
implementation. This is achieved by pre-processing the AsciiDoc files and
creating a JSON index of them with flexible metadata. With the index information
the in-browser components can do the work of filtering documents and customizing
their display.

Although there are several projects available to transform AsciiDoc into
various output formats, Stasis Reactor is built for a very specific purpose.
To address situations where a collection of losely associated documents need
to be displayed with filtering and searching, Stasis Reactor is a simple
solution.

In addition to its purpose as a simple documentation display engine, Stasis
Reactor is built with
https://github.com/cncf/toc/blob/master/DEFINITION.md[cloud native]
principles at its core. What this means is that the project is designed to be
deployed on container orchestration platforms with a minimum amount of effort.

Lastly, Stasis Reactor is designed to provide the end user with opportunities
to customize their deployments. Within this documentation you will find
examples of how to add your documents, how to customize their appearance, and
how to modify the look and feel of the document browser.

.Additional resources

* link:what-is-document-browser.html[What is the document browser?]
* link:getting-started.html[Getting Started with Stasis Reactor]
* http://asciidoc.org/[AsciiDoc documentation]
* https://asciidoctor.org/[AsciiDoctor documentation]
* https://reactjs.org/[React documentation]
