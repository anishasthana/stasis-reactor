import React from 'react';

export class ArticleIndex extends React.Component {
  render() {
    const docs = this.props.documents.map((doc, index) =>
      <li key={index}><a href={doc.url}>{doc.title}</a></li>
    );

    return (
      <div>
        <h1>Articles</h1>
        <ul>{docs}</ul>
      </div>
    );
  }
}

export default ArticleIndex;
