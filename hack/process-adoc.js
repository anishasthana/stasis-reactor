// process-adoc.js
//
// usage: node process-adoc.js <source dir> <destination dir>
//
// this script processes a directory of asciidoc (*.adoc) files into html
// using asciidoctor, it also will create a json index file of all files
// processed.
const fs = require("fs");
const Asciidoctor = require("asciidoctor.js");
const asciidoctor = Asciidoctor();
var program = require("commander");
const nunjucks = require("nunjucks");


// configure command line args
// ---------------------------

program
  .option("-s, --source <path>", "path for the source files")
  .option("-d, --destination <path>", "path to write the output files")
  .option("-t, --templates <path>", "path to template directory");


// confirm command line arguments
// ---------------------------

program.parse(process.argv);
const source = program.source;
const destination = program.destination;
const templates = program.templates;

if (source == null) {
    console.log("must specify source directory")
    process.exit(1);
}

if (destination == null) {
    console.log("must specify destination directory")
    process.exit(1);
}

if (is_dir(source) === false) {
    console.log("source does not exist or is not a directory");
    process.exit(1);
}

if (is_dir(destination) === false) {
    console.log("destination does not exist or is not a directory");
    process.exit(1);
}

if (templates != null && is_dir(templates) === false) {
    console.log("templates path does not exist or is not a directory");
    process.exit(1);
}


// configure templates
// -------------------

if (templates != null) {
  const templateConfig = {
    autoescape: false
  };
  nunjucks.configure(templateConfig);

  var converters = {};
  const tmplfiles = fs.readdirSync(templates).filter(f => f.endsWith(".html"));

  function load_and_configure_template(basename) {
    var filename = `${basename}.html`;
    if (tmplfiles.indexOf(filename) != -1) {
      var documentTemplate = fs.readFileSync(templates + "/" + filename).toString();
      converters[basename]= (node) => nunjucks.renderString(documentTemplate, {context: node});
      console.log("loading document template");
      return true;
    }
    return false;
  }

  load_and_configure_template("document");

  class TemplateConverter {
    constructor () {
      this.baseConverter = asciidoctor.Html5Converter.$new();
      this.templates = converters;
    }

    convert (node, transform, opts) {
      const template = this.templates[transform || node.node_name];
      if (template) {
        return template(node);
      }
      return this.baseConverter.convert(node, transform, opts);
    }
  }

  asciidoctor.ConverterFactory.register(new TemplateConverter(), ['html5']);
}

// process source directory files
// ------------------------------

var doclist = [];
var taglist = [];
const regex = new RegExp("adoc$");
const srcfiles = fs.readdirSync(source).filter(f => f.endsWith(".adoc"));
srcfiles.forEach(function(item) {
  console.log("converting " + item);
  var filename = source + "/" + item; // this might break on windows
  var adoc = asciidoctor.convertFile(filename, {to_dir: destination});

  // gather tags
  var tagstring = adoc.getAttribute("stasis-tags", false);
  var tags =[];
  if (tagstring !== false) {
    tags = tagstring.split(",").map(t => t.trim());
    tags.forEach(function(tag) {
      if (taglist.includes(tag) === false) {
        taglist.push(tag);
      }
    });
  }

  // create document info
  doclist.push({
    "title": adoc.getAttributes().doctitle,
    "tags": tags,
    "url": "docs/" + item.replace(regex, "html"),
    "weight": adoc.getAttribute("stasis-weight", 100)
  });
});

// sort the doc list
doclist = doclist.sort(function(a, b) {
  var A = a.title.toUpperCase();
  var B = b.title.toUpperCase();
  if (A < B) {
    return -1;
  }
  if (A > B) {
    return 1;
  }
  return 0;
}).sort(function(a, b) {
  if (a.weight < b.weight) {
    return -1;
  }
  if (a.weight > b.weight) {
    return 1;
  }
  return 0
});

var docsindex = {
  "documents": doclist,
  "tags": taglist
};

console.log("writing document index");
const contents = JSON.stringify({"index": docsindex});
var filename = destination + "/index.json";
fs.writeFileSync(filename, contents);

// here be functions
// -----------------

// check to see if directory exists
function is_dir(path) {
  if (fs.existsSync(path)) {
    let stats = fs.statSync(path);
    if (stats.isDirectory()) {
      return true;
    }
  }
  return false;
}
